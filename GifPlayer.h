#ifndef GIFPLAYER_LIBRARY_H
#define GIFPLAYER_LIBRARY_H

#include <chrono>
#include <vector>

#if defined( _MSC_VER )
struct IDirect3DDevice9;
struct IDirect3DTexture9;
#else
class IDirect3DDevice9;
class IDirect3DTexture9;
#endif

/**
 * @brief DirectX9 wrapper for library gifdec
 */
class [[maybe_unused]] GifPlayer {
	class frame_t {
	public:
		frame_t() = delete;
		explicit frame_t( struct gd_GIF *gif );
		~frame_t();

		void Initialize( struct gd_GIF *gif, IDirect3DDevice9 *pD3Ddev, bool bg = true ) noexcept;
		void Invalidate() noexcept;

		[[nodiscard]] IDirect3DTexture9 * GetTexture() const noexcept;
		[[nodiscard]] std::chrono::milliseconds GetTime() const noexcept;

	protected:
		IDirect3DTexture9 * texture = nullptr;
		std::chrono::milliseconds time{};

		bool ReleaseTexture() noexcept;

	private:
		uint8_t *raw = nullptr;
	};

public:
	GifPlayer() = delete;
	/**
	 * @brief Load gif from file
	 * @details May disable background for some gif
	 * @param file_name name of gif file
	 */
	[[maybe_unused]] explicit GifPlayer( const char *file_name );
	~GifPlayer();

	/**
	 * @brief Initialize DirectX9 textures
	 * @param pD3Ddev DirectX9 device
	 */
	[[maybe_unused]] void Initialize( IDirect3DDevice9 *pD3Ddev ) noexcept;
	/**
	 * @brief Invalidate DirectX9 textures
	 */
	[[maybe_unused]] void Invalidate() noexcept;

	enum eFrameOrder : bool { kRenderAllFrames = false, kSkipTimedOutFrames = true };
	/**
	 * @brief Process playing gif frames
	 * @details With @b kSkipTimedOutFrames used recursion to find next frame
	 * @param order frame ordering. `kRenderAllFrames` rendered all frames, but may too slow. `kSkipTimedOutFrames` faster, but may skip timed out frames
	 * @return Texture of playing frame or @b nullptr on error
	 */
	[[maybe_unused]] [[nodiscard]] IDirect3DTexture9 *ProcessPlay( eFrameOrder order = kRenderAllFrames ) const noexcept;

	/**
	 * @brief Reset loop counter for playing gif again
	 */
	[[maybe_unused]] void ResetLoop() const noexcept;

	/**
	 * @brief Check is animation loop ended and now showed last frame
	 * @return true if loop ended
	 */
	[[maybe_unused]] [[nodiscard]] bool IsLoopEnded() const noexcept;

	/**
	 * @brief Check is background rendered
	 * @return true if background enabled
	 */
	[[maybe_unused]] [[nodiscard]] bool IsDrawBg() const noexcept;
	/**
	 * @brief Toggle background
	 * @param state new state for background rendering
	 */
	[[maybe_unused]] void ToggleBg( bool state ) noexcept;
	/**
	 * @brief Toggle background without update textures
	 * @details Textures update automatically on reset device
	 * @param state new state for background rendering
	 */
	[[maybe_unused]] void ToggleBgNoUpdate( bool state ) noexcept;

	/**
	 * @brief Force update textures
	 */
	[[maybe_unused]] void Update() noexcept;

	/**
	 * @brief Get gif width
	 * @return width
	 */
	[[maybe_unused]] [[nodiscard]] uint16_t GetWidth() const noexcept;
	/**
	 * @brief Get gif height
	 * @return height
	 */
	[[maybe_unused]] [[nodiscard]] uint16_t GetHeight() const noexcept;

protected:
	std::vector<frame_t *>	frames;
	struct gd_GIF *			gif	   = nullptr;
	IDirect3DDevice9 *device = nullptr;

	bool bg = true;

	mutable size_t					  activeFrame = 0;
	mutable uint16_t				  count		  = 0;
	mutable std::chrono::milliseconds startPlay{ 0 };
	std::chrono::milliseconds		  playTime{ 0 };

	[[nodiscard]] static std::chrono::milliseconds CurrentTime() noexcept;
};

#endif //GIFPLAYER_LIBRARY_H
