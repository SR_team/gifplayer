#include "GifPlayer.h"
#include "gifdec/gifdec.h"

#include <stdexcept>

#include <cstring>
#include <d3d9.h>

using namespace std::chrono_literals;

namespace {
	class no_frames_in_gif : public std::runtime_error {
	public:
		explicit no_frames_in_gif( const char *arg ) noexcept : std::runtime_error( arg ) {}
	};
	class read_frame_error : public std::runtime_error {
	public:
		explicit read_frame_error( const char *arg ) noexcept : std::runtime_error( arg ) {}
	};
} // namespace

GifPlayer::frame_t::frame_t( struct gd_GIF *gif ) {
	raw				 = new uint8_t[gif->width * gif->height * 3];
	auto frameExists = gd_get_frame( gif );
	if ( frameExists == 1 )
		gd_render_frame( gif, raw );
	else if ( frameExists == 0 )
		throw ::no_frames_in_gif( "No frames in GIF" );
	else
		throw ::read_frame_error( "Error while reading frame from GIF" );
	time = std::chrono::milliseconds( gif->gce.delay * 10 );
}

GifPlayer::frame_t::~frame_t() {
	ReleaseTexture();
	delete[] raw;
}

void GifPlayer::frame_t::Initialize( struct gd_GIF *gif, IDirect3DDevice9 *pD3Ddev, bool bg ) noexcept {
	if ( !gif || !pD3Ddev || texture ) return;

	pD3Ddev->CreateTexture( gif->width, gif->height, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &texture, nullptr );

	D3DLOCKED_RECT LockedRect;
	texture->LockRect( 0, &LockedRect, nullptr, 0 );
	auto surfaceData = static_cast<uint8_t *>( LockedRect.pBits );
	auto pitch		 = LockedRect.Pitch;

	uint8_t *color = raw;
	for ( auto h = 0; h < gif->height; ++h ) {
		for ( auto w = 0; w < gif->width; ++w ) {
			uint32_t pixel = 0xFF'FF'FF'FF;
			if ( !gd_is_bgcolor( gif, color ) ) {
				auto pARGB = (uint8_t *)&pixel;
				pARGB[2]   = color[0]; // R
				pARGB[1]   = color[1]; // G
				pARGB[0]   = color[2]; // B
			} else if ( bg ) {
				auto bgColor = &gif->gct.colors[gif->bgindex * 3];
				auto pARGB	 = (uint8_t *)&pixel;
				pARGB[2]	 = bgColor[0]; // R
				pARGB[1]	 = bgColor[1]; // G
				pARGB[0]	 = bgColor[2]; // B
			} else
				pixel = 0;
			auto addr = surfaceData + ( h * pitch + w * sizeof( pixel ) );
			memcpy( addr, &pixel, sizeof( pixel ) );
			color += 3;
		}
	}

	texture->UnlockRect( 0 );
}

void GifPlayer::frame_t::Invalidate() noexcept {
	ReleaseTexture();
}

IDirect3DTexture9 *GifPlayer::frame_t::GetTexture() const noexcept {
	return texture;
}

std::chrono::milliseconds GifPlayer::frame_t::GetTime() const noexcept {
	return time;
}

bool GifPlayer::frame_t::ReleaseTexture() noexcept {
	if ( !texture ) return false;
	texture->Release();
	texture = nullptr;
	return true;
}

[[maybe_unused]] GifPlayer::GifPlayer( const char *file_name ) {
	gif			 = gd_open_gif( file_name );
	auto bgColor = &gif->gct.colors[gif->bgindex * 3];
	if ( bgColor[0] == 0 && bgColor[1] == 0 && bgColor[2] == 0 ) bg = false;

	while ( true ) {
		try {
			auto frame = new frame_t( gif );
			frames.push_back( frame );
		} catch ( ::no_frames_in_gif &e ) {
			break;
		} catch ( ::read_frame_error &e ) {
			gd_close_gif( gif );
			gif = nullptr;
			throw std::runtime_error( "GIF file is corrupt!" );
		}
	}
	for ( auto &&frame : frames ) playTime += frame->GetTime();
}

GifPlayer::~GifPlayer() {
	for ( auto &&frame : frames ) delete frame;
	frames.clear();
	if ( gif ) gd_close_gif( gif );
}

[[maybe_unused]] void GifPlayer::Initialize( IDirect3DDevice9 *pD3Ddev ) noexcept {
	for ( auto &&frame : frames ) frame->Initialize( gif, pD3Ddev, bg );
	device = pD3Ddev;
}

[[maybe_unused]] void GifPlayer::Invalidate() noexcept {
	for ( auto &&frame : frames ) frame->Invalidate();
}

[[maybe_unused]] IDirect3DTexture9 *GifPlayer::ProcessPlay( GifPlayer::eFrameOrder order ) const noexcept {
	if ( frames.empty() ) return nullptr;
	if ( gif->loop_count != 0 && count >= gif->loop_count ) return frames.back()->GetTexture();
	if ( startPlay + playTime < CurrentTime() ) {
		startPlay	= CurrentTime();
		activeFrame = 0;
		return frames.front()->GetTexture();
	}

	const auto frame = frames.at( activeFrame );
	if ( startPlay + frame->GetTime() > CurrentTime() ) return frame->GetTexture();
	++activeFrame;
	if ( activeFrame == frames.size() ) {
		activeFrame = 0;
		if ( gif->loop_count != 0 ) {
			count++;
			if ( count >= gif->loop_count ) return frames.back()->GetTexture();
		}
	}

	if ( order == eFrameOrder::kRenderAllFrames ) {
		startPlay = CurrentTime();
		return frames.at( activeFrame )->GetTexture();
	}
	startPlay += frame->GetTime();
	return ProcessPlay( order );
}

[[maybe_unused]] void GifPlayer::ResetLoop() const noexcept {
	count		= 0;
	activeFrame = 0;
	startPlay	= 0ms;
}

[[maybe_unused]] bool GifPlayer::IsLoopEnded() const noexcept {
	return gif->loop_count && count >= gif->loop_count;
}

[[maybe_unused]] bool GifPlayer::IsDrawBg() const noexcept {
	return bg;
}

[[maybe_unused]] void GifPlayer::ToggleBg( bool state ) noexcept {
	ToggleBgNoUpdate( state );
	Update();
}

[[maybe_unused]] void GifPlayer::ToggleBgNoUpdate( bool state ) noexcept {
	bg = state;
}

[[maybe_unused]] void GifPlayer::Update() noexcept {
	for ( auto &&frame : frames ) {
		if ( frame->GetTexture() ) {
			frame->Invalidate();
			frame->Initialize( gif, device, bg );
		}
	}
}

std::chrono::milliseconds GifPlayer::CurrentTime() noexcept {
	return std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
}

[[maybe_unused]] uint16_t GifPlayer::GetWidth() const noexcept {
	return gif->width;
}

[[maybe_unused]] uint16_t GifPlayer::GetHeight() const noexcept {
	return gif->height;
}
